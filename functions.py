from os import listdir
from os.path import isdir, isfile, splitext
from random import randint
from subprocess import run

import os
def err():
    print('Directorio invalido')
    exit()

def validate_dir(directory): 
    if not isdir(directory): err()
    else: return directory 

def enter_directory():
    directory = '/home/hecdelatorre/Pictures/r-wallpapers'
    directory = validate_dir(directory)
    return directory

def select_image(directory):
    directoryArray = listdir(directory)
    directorys = list()
    valid_extensions = ['.jpg', '.jpeg', '.png', '.bmp']
    c = 0

    for image in directoryArray: 
        if isfile(f'{directory}/{image}'): 
            extension = splitext(image)[1].lower()
            if extension in valid_extensions:
                directorys.append(f'{directory}/{image}')
                c += 1

    if len(directorys) == 0:
        exit()

    return directorys, c

def r_number(count):
    ran_number = randint(0, count - 1)
    return ran_number

def del_image():
    if isfile('/home/hecdelatorre/Pictures/wallpaper.jpg'):
        run(['rm','-f', '/home/hecdelatorre/Pictures/wallpaper.jpg'])

def set_wallpaper(image):
    run(['ln', image, '/home/hecdelatorre/Pictures/wallpaper.jpg'])
