from functions import enter_directory, select_image, r_number, del_image, set_wallpaper

def main():
    enDirectory = enter_directory()
    (image, c) = select_image(enDirectory)
    ran_number = r_number(c)
    del_image()
    set_wallpaper(image[ran_number])

if __name__ == '__main__': main()
